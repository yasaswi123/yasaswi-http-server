const http = require('http')
const path = require('path')
const fs = require('fs')
const uuid = require('uuid')

const server = http.createServer((request, response) => {
    try {
        if (request.url === '/') {
            fs.readFile(path.join(__dirname, 'public', './index.html'), 'utf-8', (err, data) => {
                if (err) {
                    throw err
                }
                else {
                    response.writeHead(200, { 'content-Type': 'text/html' })
                    response.end(data)
                }
            })
        }

        if (request.url === '/slideshow') {
            fs.readFile(path.join(__dirname, 'public', './slideshow.json'), 'utf-8', (err, data) => {
                if (err) {
                    throw err
                }
                else {
                    response.writeHead(200, { 'content-Type': 'application/json' })
                    response.end(data)
                }
            })
        }

        if (request.url === '/uuid') {
            response.writeHead(200, { 'content-Type': 'text/uuid' })
            response.end(JSON.stringify({ "uuid": uuid.v4() }))
        }

        if (request.url.includes('/status')) {
            const breaking = request.url.split('/')
            response.end(breaking[breaking.length - 1])
        }

        if (request.url.includes('/delay')) {
            const divide = request.url.split('/')
            const last = divide[divide.length - 1]
            setTimeout(() => {
                response.writeHead(200, { 'content-Type': 'text/delay' })
                response.end("Success")
            }, last * 1000)
        }
    }
    catch (error) {
        console.error(error)
    }
})

const PORT = process.env.PORT || 5000
server.listen(PORT, () => console.log(`Server running on port ${PORT}`))